﻿using Entities.Models;
using HomeCare.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
    public interface IDoctorRepository
    {
        Task<IEnumerable<Doctor>> GetAllDoctorsAsync();
        Task<Doctor> GetDoctorByIdAsync(int id);
        Task<Doctor> GetDoctorWhitDetailsAsync(int id);
        void CreatePatient(Doctor doctor);
        void UpdatePatient(Doctor doctor);
        void DeletePatient(Doctor doctor);

    }
}
