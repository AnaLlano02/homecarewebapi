﻿using HomeCare.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
    public interface IDemographicRepository
    {
        Task<IEnumerable<Demographic>> GetAllDemographicsAsync();
        Task<Demographic> GetDemographicByIdAsync(int id);
        Task<Demographic> GetDemographicWhitDetailsAsync(int id);
        void CreateDemographic(Demographic demographic);
        void UpdateDemographic(Demographic demographic);
        void DeleteDemographic(Demographic demographic);
    }
}
