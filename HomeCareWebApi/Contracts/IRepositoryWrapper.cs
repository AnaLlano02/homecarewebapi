﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCareContracts
{
   public interface IRepositoryWrapper
    {
        IUserRepository user { get; }
        IPatientRepository patient { get; }
        void save();
        
    }
}
