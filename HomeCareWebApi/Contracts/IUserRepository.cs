﻿using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCareContracts
{
    /*Interfaz que se utiliza para una entidad en especifico en este caso USER para la implementacion 
     de sus propios metodos en el controlador o donde se desee utilizar en el proyecto*/
    public interface IUserRepository 
    {
       Task <IEnumerable<User>> GetAllUsersAsync();
       Task <User> GetUserByIdAsync(int id);
       Task <User> GetUserWhitDetailsAsync(int id);
       void CreateUser(User user);
       void UpdateUser(User user);
       void DeleteUser(User user);
    }
}
