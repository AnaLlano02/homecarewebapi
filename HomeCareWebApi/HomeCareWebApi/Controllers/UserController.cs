﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Policy;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DTO;
using Entities.Models;
using HomeCareContracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace HomeCareWebApi.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public UserController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;

        }
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var users = await _repository.user.GetAllUsersAsync();
                _logger.LogInfo($"Returned all users from database.");

                var usersResults = _mapper.Map<IEnumerable<UserDTO>>(users);
                usersResults.ToList();
                return Ok(usersResults);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllUsers action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                var user = await _repository.user.GetUserByIdAsync(id);

                if (user == null)
                {
                    _logger.LogInfo($"Returned one with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned owner with id: {id}");
                    var userResult = _mapper.Map<UserDTO>(user);
                    return Ok(userResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost]
        public IActionResult CreateUser([FromBody] UserForCreationDTO user)
        {
            try
            {
                if (user == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var userEntity = _mapper.Map<User>(user);
                _repository.user.CreateUser(userEntity);
                _repository.save();
                var createdUser = _mapper.Map<UserDTO>(userEntity);

                return CreatedAtRoute("GetUserById", new { id = createdUser.UserId }, createdUser);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UserForUpdateDTO user)
        {
            try
            {
                if (user == null)
                {
                    _logger.LogError("The User object sent from client is null.");
                    return BadRequest("User object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid user object sent from client.");
                    return BadRequest();
                }
                var userEntity = await _repository.user.GetUserByIdAsync(id);
                if (userEntity == null)
                {
                    _logger.LogError("$User with id: { id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(user, userEntity);
                _repository.user.UpdateUser(userEntity);
                _repository.save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id) 
        {
            try
            {
                var user = await _repository.user.GetUserByIdAsync(id);
                if (user == null) 
                {
                    _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.user.DeleteUser(user);
                _repository.save();
                return NoContent();
            
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        /* [HttpGet]
            public IActionResult GetUsers([FromQuery] OwnerParameters ownerParameters) 
            {
                var users = _repository.user.GetAllUsersAsync();
                _logger.LogInfo($"Returned {user.c} from database");
                return Ok(users);
            }*/


    }
}

