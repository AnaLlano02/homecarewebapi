﻿using HomeCareContracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HomeCareWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
         private readonly IRepositoryWrapper _repoWrapper;
        public WeatherForecastController(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //var domesticAccounts = _repoWrapper.user.FindByCondition(x => x.UserId == 1);
            // var owners = _repoWrapper.Owner.FindAll();
            return new string[] { "value1", "value2" };
        }
    }
}

