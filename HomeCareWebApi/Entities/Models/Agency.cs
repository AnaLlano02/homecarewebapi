namespace HomeCare.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Agency")]
    public partial class Agency
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Agency()
        {
            InsuranceDetails = new HashSet<InsuranceDetail>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdAgency { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string AddressLine1 { get; set; }

        [Required]
        [StringLength(100)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }

        [Required]
        [StringLength(50)]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(20)]
        public string TelephoneNumber { get; set; }

        [StringLength(10)]
        public string Fax { get; set; }

        [Required]
        [StringLength(30)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string URL { get; set; }

        [Required]
        [StringLength(50)]
        public string SiteId { get; set; }

        [Required]
        [StringLength(50)]
        public string FederalTaxID { get; set; }

        [Required]
        [StringLength(15)]
        public string CliaId { get; set; }

        [Required]
        [StringLength(15)]
        public string ParentTIN { get; set; }

        [Required]
        [StringLength(15)]
        public string GroupNPI { get; set; }

        [Required]
        [StringLength(20)]
        public string TaxonomyCode { get; set; }

        [Required]
        [StringLength(15)]
        public string LockboxNo { get; set; }

        public int DeleteFlag { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InsuranceDetail> InsuranceDetails { get; set; }
    }
}
