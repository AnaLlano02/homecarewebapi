﻿using HomeCare.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("User")]
    public partial class User
    {

        public User()
        {
            Doctors = new HashSet<Doctor>();
            Employees = new HashSet<Employee>();
            Patients = new HashSet<Patient>();
        }

        public int UserId { get; set; }

        [Required]
        [StringLength(60)]

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "PassWord")]
        public string Pasword { get; set; }

        public int RoleId { get; set; }


       public virtual ICollection<Doctor> Doctors { get; set; }
       public virtual ICollection<Employee> Employees { get; set; }
       public virtual ICollection<Patient> Patients { get; set; }

       public virtual Role Role { get; set; }
    }
}
