namespace HomeCare.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("InsuranceDetail")]
    public partial class InsuranceDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdInsuranceDetail { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupNo { get; set; }

        [Required]
        [StringLength(50)]
        public string SubscriberNo { get; set; }

        public int InsOrder { get; set; }

        public short DeleteFlag { get; set; }

        [Required]
        [StringLength(20)]
        public string PaymentSource { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }

        [Required]
        [StringLength(20)]
        public string InsType { get; set; }

        [Required]
        [StringLength(5)]
        public string AssignBenefits { get; set; }

        public int SeqNo { get; set; }

        [Required]
        [StringLength(10)]
        public string PtSigSource { get; set; }

        [Required]
        [StringLength(20)]
        public string PayerClaimOfficeNo { get; set; }

        [Required]
        [StringLength(5)]
        public string SuppInsInd { get; set; }

        [Required]
        [StringLength(10)]
        public string EncEligibilityStatus { get; set; }

        [Required]
        [StringLength(200)]
        public string EligibilityMessage { get; set; }

        [Required]
        [StringLength(35)]
        public string InsuredAltLName { get; set; }

        [Required]
        [StringLength(35)]
        public string InsuredAltFName { get; set; }

        [Required]
        [StringLength(50)]
        public string InsuredAltMiddleInitial { get; set; }

        [Required]
        [StringLength(200)]
        public string Notes { get; set; }

        public int IdAgency { get; set; }

        public int IdPatient { get; set; }

        public int IdInsurance { get; set; }

        public virtual Agency Agency { get; set; }

        public virtual Insurance Insurance { get; set; }

        public virtual Patient Patient { get; set; }
    }
}
