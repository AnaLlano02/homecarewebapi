﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Patient
{
    public class PatientForUpdateDTO
    {
        public string FirstName { get; set; }
        public string Middle { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Gender { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string eMail { get; set; }
        public string Status { get; set; }


    }
}
