﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTO.Patient
{
    public class PatientDTO
    {
  
        public int IdPatient { get; set; }

        
        public string FirstName { get; set; }

     
        public string Middle { get; set; }

   
        public string LastName { get; set; }


        public DateTime DateOfBirth { get; set; }


        public string SocialSecurityNumber { get; set; }

     
        public string Gender { get; set; }

       
        public string Phone1 { get; set; }

 
        public string Phone2 { get; set; }

     
        public string eMail { get; set; }


        public string Status { get; set; }

        public int UserId { get; set; }

        public int IdDemographic { get; set; }
    }
}
