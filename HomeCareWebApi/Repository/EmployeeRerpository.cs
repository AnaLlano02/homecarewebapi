﻿using Entities;
using HomeCare.Core.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public class EmployeeRerpository : RepositoryBase<Employee>, IEmployeeRepository
    {

        public EmployeeRerpository(RepositoryContext context)
                : base(context)
        {
        }

        public void CreateEmployee(Employee employee)
        {
            Create(employee);
        }

        public void DeleteEmployee(Employee employee)
        {
            Delete(employee);
        }

        public async Task<IEnumerable<Employee>> GetAllEmployeesAsync()
        {
            return await FindAll()
                .OrderBy(emp => emp.FirstName)
                .ToListAsync();
        }

        public async Task<Employee> GetEmployeeByIdAsync(int id)
        {

            return await FindByCondition(emp => emp.IdEmployees.Equals(id))
                    .Include(us => us.IdUser)
                    .Include(role => role.IdRole)
                    .FirstOrDefaultAsync();
        }

        public Task<Employee> GetUserWhitDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }
    }
}
