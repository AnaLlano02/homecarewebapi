﻿using Entities;
using HomeCare.Core.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Repository
{
    public class DoctorRepository : RepositoryBase<Doctor>, IDoctorRepository
    {
        public DoctorRepository(RepositoryContext context)
        : base(context)
        { 
        }

        public void CreatePatient(Doctor doctor)
        {
            Create(doctor);
        }

        public void DeletePatient(Doctor doctor)
        {
            Delete(doctor);
        }

        public async Task<IEnumerable<Doctor>> GetAllDoctorsAsync()
        {
            return await FindAll()
                .OrderBy(doc => doc.FirstName)
                .ToListAsync();
        }

        public async Task<Doctor> GetDoctorByIdAsync(int id)
        {

            return await FindByCondition(doc =>doc.IdDoctor.Equals(id))
                    .Include(doc => doc.IdDoctor)
                    .FirstOrDefaultAsync();
        }

        public async Task<Doctor> GetDoctorWhitDetailsAsync(int id)
        {
            return await FindByCondition(doc => doc.IdDoctor.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdatePatient(Doctor doctor)
        {
            Update(doctor);
        }
    }
}
