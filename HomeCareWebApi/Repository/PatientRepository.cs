﻿using Entities;
using HomeCare.Core.Models;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PatientRepository : RepositoryBase<Patient>, IPatientRepository
    {
        public PatientRepository(RepositoryContext context)
          : base(context)
        {

        }

        public void CreatePatient(Patient patient)
        {
            Create(patient);
        }

        public void DeletePatient(Patient patient)
        {
            Delete(patient);
        }

        public async Task<IEnumerable<Patient>> GetAllPatientsAsync()
        {
            return await FindAll()
                .OrderBy(pa => pa.FirstName)
                .ToListAsync();
        }

        public async Task<Patient> GetPatienByIdAsync(int id)
        {
            return await FindByCondition(patient => patient.IdPatient.Equals(id))
                    .Include(us => us.UserId)
                    .FirstOrDefaultAsync();
        }

        public async Task<Patient> GetPatientWhitDetailsAsync(int id)
        {
            return await FindByCondition(patient => patient.IdPatient.Equals(id)).FirstOrDefaultAsync();
        }

        public void UpdatePatient(Patient patient)
        {
            Update(patient);
        }
    }
}
