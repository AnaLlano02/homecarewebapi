﻿using Entities;
using HomeCareContracts;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly RepositoryContext _repContext;
        private IUserRepository _user;
        private IPatientRepository _patient; 
        
        public IUserRepository user 
        {
            get {
                if (_user == null) 
                {
                    _user = new UserRepository(_repContext);
                }
                return _user;
            }
        }

        public IPatientRepository patient 
        {
            get {
                if (_patient == null) 
                {
                    _patient = new PatientRepository(_repContext);
                }
                return _patient;
            }
        }

        public RepositoryWrapper (RepositoryContext repositoryContext) 
        {
            _repContext = repositoryContext;
        }

        public void save()
        {
            _repContext.SaveChanges();
        }

      
    }
}
